package kame.tagapi;

import org.bukkit.entity.Entity;

import java.util.Objects;

/**
 * エンティティのNBTを取得、編集する為の機能を提供します。
 */
public class TagEntity {

    private final Entity entity;
    private final TagSection section;

    /**
     * 引数に指定したEntityからこのインスタンスを初期化します。
     * @param entity NBTを取得、編集するEntityオブジェクト
     */
    public TagEntity(Entity entity) {
        this.entity = entity;
        var nbt = Mapper.getEntityData(entity);
        section = Mapper.toTag(nbt) instanceof TagSection data ? data : null;
    }


    /**
     * このエンティティの状態をNBTデータとして取得します。
     * このNBTで編集された内容はsaveメソッドを呼び出すまで反映されません。
     * @see #save()
     * @return 取得出来たNBT
     */
    public TagSection getSection() {
        return section;
    }

    /**
     * このインスタンスのNBTデータが元の状態から変更されたかを取得します。
     * @return NBTが変更されている場合はtrue、未変更の場合はfalse
     */
    public boolean hasChanged() {
        var nbt = Mapper.getEntityData(entity);
        var change = Mapper.toNms(section);
        return !Objects.equals(nbt, change);
    }

    /**
     * このインスタンスのNBTオブジェクトで編集した内容を内包しているEntityに反映します。
     * @return 保存前とNBTに差異があった場合はtrue、変更されなかった場合はfalse
     */
    public boolean save() {
        if (hasChanged()) {
            Mapper.setEntityData(entity, Mapper.toNms(section));
            return true;
        } else {
            return false;
        }
    }
}
