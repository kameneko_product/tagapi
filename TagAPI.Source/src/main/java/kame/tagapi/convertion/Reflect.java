package kame.tagapi.convertion;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Optional;

class Reflect {

    public static <T> Optional<Constructor<T>> getAnnotatedConstructors(Class<T> type, Class<?>[] classes) throws ConvertException {
        var constructors = type.getDeclaredConstructors();
        var annotated = Arrays.stream(constructors)
                .filter(x -> x.isAnnotationPresent(ConvertConstructors.class))
                .filter(x -> classes == null || Arrays.equals(x.getParameterTypes(), classes))
                .toList();
        if (annotated.size() == 1) {
            return Optional.of(Internal.getConstructor(type, annotated.get(0).getParameterTypes()));
        } else if (annotated.size() > 1) {
            throw new ConvertException("Must have no more than one annotated constructor. detectives:" + annotated.size());
        }
        return Arrays.stream(constructors)
                .filter(x -> classes == null || Arrays.equals(x.getParameterTypes(), classes))
                .findFirst()
                .map(Constructor::getParameterTypes)
                .map(x -> Internal.getConstructor(type, x));
    }

    public static String getKeyName(Field field) {
        return Optional.ofNullable(field.getAnnotation(KeyName.class)).map(KeyName::value).orElse(field.getName());
    }

    public static String getKeyName(Parameter param) {
        return Optional.ofNullable(param.getAnnotation(KeyName.class)).map(KeyName::value).orElse(param.getName());
    }

}