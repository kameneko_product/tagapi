# TagAPI
このAPIはSpigotプラグインでのNBTタグの操作を行う機能を提供します。

# コード例
#### 値の設定
```java
// ItemStackの宣言
var item = new ItemStack(Material.BONE); 
// ItemStackからTagItemインスタンスを初期化
var tagItem = new TagItem(item); 
// NBTデータを取得し、Unbreakableに1bを設定
tagitem.getSection().set("Unbreakable", new TagByte(1)); 
// 変更したNBTデータの反映
tagItem.save();
```
#### NBTの任意オブジェクトへの変換(値の変更は反映されません)
```java
record Display(String name, List<String> lore) { }
// ~~ (省略) ~~
TagSection section = tagItem.getSection();
// 「T convertTo(Class<T>)」でNBTをオブジェクト化
Display display = section.convertTo(Display.class);
```

# 依存関係の追加方法
<details><summary>Maven: pom.xmlに追加してください (クリックで表示) </summary>

#### repositories
```xml
<repositories>
    <!-- repositoriesにkameneko_productの場所を追加 -->
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/54021868/-/packages/maven</url>
    </repository>
</repositories>
```
#### dependencies
```xml
<dependencies>
    <!-- dependenciesにTagAPIを追加 -->
    <dependency>
        <groupId>kame</groupId>
        <artifactId>TagAPI</artifactId>
        <version>1.0.0</version>
        <scope>compile</scope>
    </dependency>
</dependencies>
```
</details>
<details>
<summary>Gradle: build.gradleに追加してください (クリックで表示)  </summary>

#### repositories
```groovy
repositories {
    // repositoriesにkameneko_productの場所を追加
    maven {
        url = uri('https://gitlab.com/api/v4/groups/54021868/-/packages/maven')
    }
}
```
#### dependencies
```groovy
dependencies {
    compileOnly 'kame:TagAPI:1.0.0'
}
```

</details>

<details>
<summary>直接プロジェクトに取り込む場合 (クリックで表示) </summary>

[Packages and registries](https://gitlab.com/kameneko_product/tagapi/-/packages)より必要なバージョンのjarをダウンロードしプロジェクトへ追加してください。
</details>

# 補足説明
### TagSection::convertToの制約について
このメソッドで使用では以下に当てはまる型が使用できます。
- String型
- String型の配列
- 値(byte, short, int, long, float, double)
- 値(byte, short, int, long, float, double)の配列
- 任意のオブジェクトまたはレコード型 (class)
- オブジェクトまたはレコードの配列
- 第2引数のConvertOptionsの型に登録したインターフェース
- List&lt;T&gt;やList&lt;? extends T&gt; (デフォルトではArrayListとして扱われます)
- Map&lt;String, T&gt; (デフォルトではHashMapとして扱われます)

#### :warning: 注意点 :warning:
全てのクラスパターンには対応できていません(staticでない入れ子クラスなど)

そのうち出来るようにしたい。

変換できない例
```java
// hugaフィールドの型をジェネリクスで渡す
class Hoge<T> {
    T value; // <-ここは例外なく通る
    class Huga {
        T piyo; // 入れ子クラスの親のジェネリック型を参照出来ない
    }
}
```
現状では任意クラスの型に複雑なジェネリクスを含まないでください。(recordも同様)
```java
class Hoge {
    public Huga huga;
}
class Huga {
    public String piyo;
}
```

※キーが動的で型が統一できない場合はMap&lt;String, Object&gt;を使用してください。

### 提供しているクラスの変換先
|APIクラス|変換先|
|----|----|
|TagSection|任意型(class, record), Map&lt;String, T&gt;|
|TagList|Iterable&lt;T&gt;,Collection&lt;T&gt;,List&lt;T&gt;|
|TagByteArray|byte[], Byte[]|
|TagIntArray|int[], Integer[]|
|TagLongArray|long[], Long[]|
|TagString|String|
|その他(TagInt等)|対応するプリミティブ型またはラッパー型|
