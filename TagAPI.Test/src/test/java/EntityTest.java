import kame.tagapi.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.util.Vector;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.concurrent.FutureTask;

public class EntityTest {

    static {
        _ServerMain.waitInitialize();
    }

    static EntityType[] types() {
        return Arrays.stream(EntityType.values())
                .filter(EntityType::isSpawnable)
                .sorted(Comparator.comparing(Enum::name))
                .toArray(EntityType[]::new);
    }

    @ParameterizedTest
    @MethodSource("types")
    void test(EntityType type) {
        // 全Entityを対象にTagEntityを作成するテスト
        var world = Bukkit.getWorlds().stream().findFirst().orElseThrow();
        var loc = Vector.getRandom().multiply(100).toLocation(world);
        var future = new FutureTask<>(() -> test(loc, type));
        var plugin = Objects.requireNonNull(Bukkit.getPluginManager().getPlugin("Dummy"));
        Bukkit.getScheduler().runTask(plugin, future);
        while(!future.isDone() && !future.isCancelled()) Thread.yield();
        Assertions.assertDoesNotThrow(() -> future.get());
    }

    Exception test(Location loc, EntityType type) {
        try {
            var world = Objects.requireNonNull(loc.getWorld());
            Entity entity;
            try {
                entity = world.spawnEntity(loc, type, true);
            }catch (IllegalArgumentException e){
                System.out.println("スポーン出来ないエンティティ");
                return null;
            }
            var tag = new TagEntity(entity);
            var sec = tag.getSection();
            System.out.println("初期値 (" + type + ") " + sec);
            sec.put("UUID", new TagIntArray(new int[]{0, 0, 0, 0}));
            sec.put("Air", new TagShort((short)0));
            tag.save();
            var valid = new TagEntity(entity).getSection();
            System.out.println("書込値 (" + type + ") " + sec);
            System.out.println("読取値 (" + type + ") " + valid);
            var validate = valid.get("UUID") instanceof TagIntArray d ? d.data() : new int[0];
            Assertions.assertArrayEquals(new int[]{0, 0, 0, 0}, validate);
            entity.remove();
            return null;
        } catch (Exception e) {
            return e;
        }
    }
}
