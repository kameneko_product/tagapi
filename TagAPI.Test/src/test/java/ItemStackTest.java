import kame.tagapi.TagInt;
import kame.tagapi.TagItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;

public class ItemStackTest {

    static {
        _ServerMain.waitInitialize();
    }

    @SuppressWarnings("deprecation")
    static Material[] types() {
        return Arrays.stream(Material.values())
                .filter(x -> !x.toString().startsWith(Material.LEGACY_PREFIX))
                .toArray(Material[]::new);
    }

    @ParameterizedTest
    @MethodSource("types")
    void test(Material type) {
        // 全Materialを対象にTagItemを作成するテスト
        System.out.println("初期値 (" + type + ") " + new TagItem(new ItemStack(type)).getSection());
        var test = "Test." + type;
        var item = new ItemStack(type);
        var tagItem = new TagItem(item);
        tagItem.getSection().put(test, new TagInt(1));
        tagItem.save();
        var sec = new TagItem(item).getSection();
        System.out.println("書込値 (" + type + ") " + tagItem.getSection());
        System.out.println("読込値 (" + type + ") " + sec);
        Assertions.assertEquals(1, sec.get(test) instanceof TagInt i ? i.data() : 0);
    }
}
