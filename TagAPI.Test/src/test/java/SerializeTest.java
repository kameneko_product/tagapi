import kame.tagapi.*;
import kame.tagapi.convertion.KeyName;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class SerializeTest {

    static {
        _ServerMain.waitInitialize();
    }

    @Test
    void byteTest() {
        var data = new Object() {
            private final byte test = 1;
        };
        var section = new TagSection(Map.of("test", new TagByte(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void shortTest() {
        var data = new Object() {
            private final short test = 1;
        };
        var section = new TagSection(Map.of("test", new TagShort(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void intTest() {
        var data = new Object() {
            private final int test = 1;
        };
        var section = new TagSection(Map.of("test", new TagInt(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void longTest() {
        var data = new Object() {
            private final long test = 1;
        };
        var section = new TagSection(Map.of("test", new TagLong(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void floatTest() {
        var data = new Object() {
            private final float test = 1;
        };
        var section = new TagSection(Map.of("test", new TagFloat(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void doubleTest() {
        var data = new Object() {
            private final double test = 1;
        };
        var section = new TagSection(Map.of("test", new TagDouble(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void byteArrayTest() {
        var data = new Object() {
            private final byte[] test = {1, 2, 3};
        };
        var section = new TagSection(Map.of("test", new TagByteArray(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertArrayEquals(data.test, result.test);
    }

    @Test
    void intArrayTest() {
        var data = new Object() {
            private final int[] test = {1, 2, 3};
        };
        var section = new TagSection(Map.of("test", new TagIntArray(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertArrayEquals(data.test, result.test);
    }

    @Test
    void longArrayTest() {
        var data = new Object() {
            private final long[] test = {1, 2, 3};
        };
        var section = new TagSection(Map.of("test", new TagLongArray(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertArrayEquals(data.test, result.test);
    }

    @Test
    void stringTest() {
        var data = new Object() {
            private final String test = "Test";
        };
        var section = new TagSection(Map.of("test", new TagString(data.test)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void listTest() {
        var data = new Object() {
            private final List<String> test = List.of("Test");
        };
        var section = new TagSection(Map.of("test", new TagList(data.test.stream().map(TagString::new).toList())));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }

    @Test
    void sectionTest() {
        var data = new Object() {
            private final Map<String, String> test = Map.of("TestKey", "TestValue");
        };
        var collect = data.test.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, x -> new TagString(x.getValue())));
        var section = new TagSection(Map.of("test", new TagSection(collect)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data.test, result.test);
    }


    @Test
    void recordTypeTest() {
        record RecordData(byte data1, int data2) {
        }
        var data = new RecordData((byte) 1, 127);
        var section = new TagSection(Map.of("data1", new TagByte(data.data1), "data2", new TagInt(data.data2)));
        var result = section.convertTo(RecordData.class);
        Assertions.assertEquals(data, result);

    }

    @Test
    void wildcardTypeTest() {
        record TypeData(String record) {
        }
        record TypeData2(List<? extends String> data1, Map<String, ? extends TypeData> data2) {
        }

        var data = new TypeData2(List.of("test_test"), Map.of("test", new TypeData("Test")));
        var collect = data.data2
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, x -> new TagSection(Map.of("record", new TagString(x.getValue().record())))));
        var section = new TagSection(
                Map.of("data1", new TagList(data.data1.stream().map(TagString::new).toList()), "data2", new TagSection(collect)));
        var result = section.convertTo(data.getClass());
        Assertions.assertEquals(data, result);
    }


    @Test
    void testSerialize() {
        ItemStack item = new ItemStack(Material.MUSIC_DISC_CAT);
        var meta = Optional.ofNullable(item.getItemMeta());
        meta.ifPresent(x -> x.setDisplayName("Neko"));
        meta.ifPresent(x -> x.setLore(List.of("A", "B")));
        meta.ifPresent(x -> x.setUnbreakable(true));
        meta.ifPresent(x -> x.addItemFlags(ItemFlag.values()));
        var attr = new AttributeModifier("", 10, AttributeModifier.Operation.ADD_NUMBER);
        meta.ifPresent(x -> x.addAttributeModifier(Attribute.GENERIC_ARMOR, attr));
        meta.ifPresent(item::setItemMeta);
        item.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 5);
        var tag = new TagItem(item);
        var root = tag.getSection();
        var section = root.createSection("custom");
        if (section != null) {
            section.put("a", new TagString("neko"));
            section.put("b", new TagString("neko_neko"));
            section.put("c", new TagString("neko_neko_neko"));
        }
        ItemStackData data1 = root.convertTo(ItemStackData.class);
        Assertions.assertEquals(data1.display.name, "{\"extra\":[{\"text\":\"Neko\"}],\"text\":\"\"}");
        Assertions.assertEquals(data1.display.lore, List.of("{\"extra\":[{\"text\":\"A\"}],\"text\":\"\"}", "{\"extra\":[{\"text\":\"B\"}],\"text\":\"\"}"));
        Assertions.assertEquals(data1.unbreakable, 1);
        if (_ServerMain.version.equalsOrLater("1.19.4")) {
            Assertions.assertEquals(data1.hideFlags, -1);
        } else {
            Assertions.assertEquals(data1.hideFlags, 127);
        }
        Assertions.assertEquals(data1.enchantments, List.of(new ItemStackData.EnchantmentData((short)5, "minecraft:power")));
        Assertions.assertEquals(data1.custom, new ItemStackData.CustomData("neko", "neko_neko", "neko_neko_neko"));
        Assertions.assertEquals(data1.attributeModifiers.size(), 1);
    }

    private static class ItemStackData {
        public DisplayData<? extends String, String> display;
        public byte unbreakable;
        public int hideFlags;
        public List<? extends EnchantmentData> enchantments;
        public CustomData custom;

        public List<? extends AttributeModifier> attributeModifiers;

        public static class DisplayData<K, S> {
            public K name;
            public List<? extends S> lore;

        }

        public record CustomData(String a, String b, String c) {
        }

        public record EnchantmentData(@KeyName("lvl") short level, String id) {

        }

        public record AttributeModifier(double amount, String attributeName, int operation, int[] uuid, String name) {

        }
    }
}
