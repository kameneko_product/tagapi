import kame.tagapi.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.stream.IntStream;

public class TagSectionTest {

    static {
        _ServerMain.waitInitialize();
    }

    private TagSection create(String key, TagBase value) {
        var section = new TagSection();
        section.put(key, value);
        return section;
    }

    @Test
    void testSectionReadByte() {
        var section = create("byte", new TagByte((byte) 0));
        var result = section.get("byte") instanceof TagByte tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadShort() {
        var section = create("short", new TagShort((short) 0));
        var result = section.get("short") instanceof TagShort tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadInt() {
        var section = create("int", new TagInt(0));
        var result = section.get("int") instanceof TagInt tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadLong() {
        var section = create("long", new TagLong(0));
        var result = section.get("long") instanceof TagLong tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadFloat() {
        var section = create("float", new TagFloat(0));
        var result = section.get("float") instanceof TagFloat tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadDouble() {
        var section = create("double", new TagDouble(0));
        var result = section.get("double") instanceof TagDouble tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadByteArray() {
        var section = create("byte[]", new TagByteArray(new byte[0]));
        var result = section.get("byte[]") instanceof TagByteArray tag && Arrays.equals(tag.data(), new byte[0]);
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadIntArray() {
        var section = create("int[]", new TagIntArray(new int[0]));
        var result = section.get("int[]") instanceof TagIntArray tag && Arrays.equals(tag.data(), new int[0]);
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadLongArray() {
        var section = create("long[]", new TagLongArray(new long[0]));
        var result = section.get("long[]") instanceof TagLongArray tag && Arrays.equals(tag.data(), new long[0]);
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadList() {
        var section = create("List", new TagList());
        var result = section.get("List") instanceof TagList tag && tag.size() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testSectionReadSection() {
        var section = create("Section", new TagSection());
        var result = section.get("Section") instanceof TagSection tag && tag.size() == 0;
        Assertions.assertTrue(result);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
    void testGetSize(int n) {
        var section = new TagSection();
        IntStream.range(0, n).mapToObj(TagInt::new).forEach(x -> section.put(x.toString(), x));
        var result = section.size() == n;
        Assertions.assertTrue(result);
    }

    @Test
    void testReplace() {
        var section = create("replace", new TagSection());
        section.put("replace", new TagInt(0));
        var result = section.get("replace") instanceof TagInt tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }
}
