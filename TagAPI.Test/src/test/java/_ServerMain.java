import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.craftbukkit.Main;

import java.util.Arrays;
import java.util.stream.Stream;

public final class _ServerMain {

    static {
        Main.main(new String[]{"nogui", "-p25566"});
    }

    public static SemanticVersion version;
    public static String revision;
    public static Server get() {
        return Bukkit.getServer();
    }

    public static void waitInitialize() {
        //noinspection ConstantConditions サーバーの起動待ち
        while (Bukkit.getServer() == null) Thread.yield();
        while (!Bukkit.getPluginManager().isPluginEnabled("Dummy")) Thread.yield();
        var ver = Bukkit.getBukkitVersion().split("-");
        version = new SemanticVersion(ver[0]);
        revision = ver[1];
    }

    public record SemanticVersion(int major, int miner, int patch) {
        private SemanticVersion(int... version) {
            this(version[0], version[1], version[2]);
        }
        private SemanticVersion(Stream<String> version) {
            this(version.mapToInt(Integer::parseInt).toArray());
        }
        public SemanticVersion(String version) {
            this(Arrays.stream(version.split("\\.")));
        }

        public boolean equalsOrLater(String version) {
            return equalsOrLater(new SemanticVersion(version));
        }

        public boolean equalsOrLater(SemanticVersion version) {
            if (major > version.major) return true;
            if (major < version.major) return false;
            if (miner > version.miner) return true;
            if (miner < version.miner) return false;
            if (patch > version.patch) return true;
            return patch >= version.patch;
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) return true;
            if (other == null || getClass() != other.getClass()) return false;
            SemanticVersion that = (SemanticVersion) other;
            return major == that.major && miner == that.miner && patch == that.patch;
        }

        @Override
        public int hashCode() {
            int result = major;
            result = 31 * result + miner;
            result = 31 * result + patch;
            return result;
        }
    }
}
