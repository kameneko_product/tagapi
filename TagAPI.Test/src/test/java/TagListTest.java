import kame.tagapi.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.stream.IntStream;

public class TagListTest {

    static {
        _ServerMain.waitInitialize();
    }

    private TagList create(TagBase value) {
        var list = new TagList();
        list.add(value);
        return list;
    }

    @Test
    void testReadByte() {
        var list = create(new TagByte((byte) 0));
        var result = list.get(0) instanceof TagByte tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testReadShort() {
        var list = create(new TagShort((short) 0));
        var result = list.get(0) instanceof TagShort tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testReadInt() {
        var list = create(new TagInt(0));
        var result = list.get(0) instanceof TagInt tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testReadLong() {
        var list = create(new TagLong(0));
        var result = list.get(0) instanceof TagLong tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testReadFloat() {
        var list = create(new TagFloat(0));
        var result = list.get(0) instanceof TagFloat tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testReadDouble() {
        var list = create(new TagDouble(0));
        var result = list.get(0) instanceof TagDouble tag && tag.data() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testReadByteArray() {
        var list = create(new TagByteArray(new byte[0]));
        var result = list.get(0) instanceof TagByteArray tag && Arrays.equals(tag.data(), new byte[0]);
        Assertions.assertTrue(result);
    }

    @Test
    void testReadIntArray() {
        var list = create(new TagIntArray(new int[0]));
        var result = list.get(0) instanceof TagIntArray tag && Arrays.equals(tag.data(), new int[0]);
        Assertions.assertTrue(result);
    }

    @Test
    void testReadLongArray() {
        var list = create(new TagLongArray(new long[0]));
        var result = list.get(0) instanceof TagLongArray tag && Arrays.equals(tag.data(), new long[0]);
        Assertions.assertTrue(result);
    }

    @Test
    void testReadList() {
        var list = create(new TagList());
        var result = list.get(0) instanceof TagList tag && tag.size() == 0;
        Assertions.assertTrue(result);
    }

    @Test
    void testReadSection() {
        var list = create(new TagSection());
        var result = list.get(0) instanceof TagSection tag && tag.size() == 0;
        Assertions.assertTrue(result);
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
    void testGetSize(int n) {
        var list = new TagList();
        IntStream.range(0, n).mapToObj(TagInt::new).forEach(list::add);
        var result = list.size() == n;
        Assertions.assertTrue(result);
    }

    @Test
    void failMultiTypes() {
        var list = create(new TagInt(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> list.add(new TagLong(0)));
        var result = list.size() == 1;
        Assertions.assertTrue(result);
    }
}
